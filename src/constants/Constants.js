const constants = {
    Description: "Description",
    Search: "Search",
    Add: "Add",
    EditTask: "Edit Task",
    DeleteTask: "Delete Task",
    NoItem: "No items in task",
    LimitExceed: "Limit Exceeded 50000",
    Category: "Category",
    Select: "Select",
    Food: "food & dinning",
    Utility: "utility",
    Shopping: "shopping",
    Education: "education",
    Travel: "travel",
    Amount: "Amount"
}

export default constants