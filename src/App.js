import logo from './logo.svg';
import Tasks from '../src/tasks/Tasks';
import './App.css';
import {Provider} from 'react-redux';
import store from './store';

function App() {
  return (
    <Provider store={store}>
      <Tasks />
    </Provider>
  );
}

export default App;
