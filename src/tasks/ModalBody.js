import React from 'react'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {FormControl, Input, InputLabel} from '@material-ui/core';
import constants from "../constants/Constants";
import ButtonComponent from './Button';

function getModalStyle() {

    return {
      top: `45%`,
      left: `45%`,
      transform: `translate(-45%, -45%)`,
    };
  }
  
  const useStyles = makeStyles((theme) => ({
    paper: {
      position: 'absolute',
      width: 900,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  }));
function ModalBody(props) {
    const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [description, setDescription] = useState(props.description);
  const [category, setCategory] = useState(props.category);
  const [amount, setAmount] = useState(props.amount);
  const [endDate, setEndDate] = useState(props.endDate);
    return (
        <div style={modalStyle} className={classes.paper}>
      <div id="addTask" style={{marginLeft: 40, marginTop: 20, flex: 1, justifyContent: "space-between", display: "flex", alignSelf: "center"}}>
        <FormControl style={{width: "20%"}}>
          <InputLabel>{constants.Description}</InputLabel>
          <Input
              name="description"
              style={{width: "100%"}}
              type="text"
              onChange={event =>
                setDescription(event.target.value)
              }
              value={description}
          />
        </FormControl>
        <FormControl style={{width: 100}}>
          <InputLabel >{constants.Category}</InputLabel>
          <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={category}
              onChange={(e)=>setCategory(e.target.value)}
          >
            <MenuItem value={constants.Select}>{constants.Select}</MenuItem>
            <MenuItem value={constants.Food}>{constants.Food}</MenuItem>
            <MenuItem value={constants.Utility}>{constants.Utility}</MenuItem>
            <MenuItem value={constants.Shopping}>{constants.Shopping}</MenuItem>
            <MenuItem value={constants.Education}>{constants.Education}</MenuItem>
            <MenuItem value={constants.Travel}>{constants.Travel}</MenuItem>
          </Select>
        </FormControl>
        
        <FormControl style={{width: "20%"}}>
          <InputLabel>{constants.Amount}</InputLabel>
          <Input
              name="amount"
              style={{width: "100%", appearance: 'none', }}
              pattern="[0-9]*"
              type="number"
              onInput={event =>{
                if (!/[0-9]/.test(event.key)) {
                  event.preventDefault();
                console.log('isValid')
                }
                return (event.target.validity.valid)?setAmount(event.target.value):undefined}
              }
              value={amount}
          />
        </FormControl>
        
        <FormControl style={{width: "20%", marginTop: 15}}>
          <Input
            name="accountName"
            style={{width: "100%"}}
            type="date"
            onChange={event =>
              setEndDate(event.target.value)
            }
            value={endDate}
          />
        </FormControl>
        
        
        </div>
        <ButtonComponent style={{marginLeft: 40, marginTop: 40}} color="primary" click={()=>{
            props.handleClose();
            props.handleSubmit(description, category, amount, endDate, props.index);
            }} text={props.buttonText} />
              
    </div>
    )
}

export default ModalBody
