import React from 'react'
import {Button} from '@material-ui/core'

export default function ButtonComponent(props) {
    return (
        <Button style={props.style} variant="contained" color={props.color} onClick={()=>{props.click()}}>
            {props.text}
        </Button>
    )
}
