import React from 'react';
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import Typography from '@material-ui/core/Typography';
import {connect} from 'react-redux';

function OppositeContentTimeline(props) {
    let sortedByDates = props.items?props.items.sort((a, b) => new Date(...a.endDate.split('/').reverse()) - new Date(...b.endDate.split('/').reverse())):[];
  return (
    <React.Fragment>
      <Timeline align="alternate">
          {sortedByDates.map((item) => {
              return <TimelineItem>
              <TimelineOppositeContent>
                <Typography color="textSecondary">{item.endDate}</Typography>
              </TimelineOppositeContent>
              <TimelineSeparator>
                <TimelineDot />
                <TimelineConnector />
              </TimelineSeparator>
              <TimelineContent>
                <Typography>{item.description}</Typography>
                <Typography>₹ {item.amount}</Typography>
              </TimelineContent>
            </TimelineItem>    
          })}
              </Timeline>
    </React.Fragment>
  );
}

const mapStateToProps = state => ({
    items: state.posts.items,
})

export default connect(mapStateToProps)(OppositeContentTimeline)