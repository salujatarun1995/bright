import React from 'react';
import Modal from '@material-ui/core/Modal';
import ModalBody from './ModalBody';

export default function SimpleModalEdit(props) {
  return (
    <div>
      <Modal
        open={props.openModal}
        onClose={()=>props.handleClose()}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <ModalBody 
          description={props.description}
          category={props.category}
          amount={props.amount}
          endDate={props.endDate}
          handleSubmit={props.handleSubmit}
          handleClose={props.handleClose} 
          index= {props.index}
          buttonText={props.buttonText}
        />
      </Modal>
    </div>
  );
}