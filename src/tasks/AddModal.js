import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import {useRef} from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {FormControl, Input, InputLabel, Button} from '@material-ui/core';
import ModalBody from './ModalBody';

export default function SimpleModalAdd(props) {

  return (
    <div>
      <Modal
        open={props.openModal}
        onClose={()=>props.handleClose()}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <ModalBody 
          description={props.description}
          category={props.category}
          amount={props.amount}
          endDate={props.endDate}
          handleSubmit={props.handleSubmit}
          handleClose={props.handleClose} 
          index= {props.index}
          buttonText={props.buttonText}
        />
      </Modal>
    </div>
  );
}