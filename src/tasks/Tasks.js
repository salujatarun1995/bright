import React from 'react'
import {FormControl, Input, InputLabel, Typography, Snackbar, } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import SimpleModalEdit from './EditModal';
import SimpleModalAdd from './AddModal';
import {useState, useRef} from 'react';
import {connect} from 'react-redux';
import {addBill, editBill, deleteBill, filterBill, longPressedBill} from '../actions/postActions'
import OppositeContentTimeline from './timeline';
import constants from '../constants/Constants'
import ButtonComponent from './Button';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function Tasks(props) {
  const valueSearched = useRef('');
  
  const [openModal, setOpenModal] = useState(false);
  const [open, setOpen] = useState(false);
  const [filterTasks, setfilterTasks] = useState([]);
  const [longPressedTasks, setLongPressedTasks] = useState([]);
  const [navigate, setNavigation] = useState(false);
  const [editIndex, setEditIndex] = useState(-1);
  const [limit, setLimit] = useState(0);
  const [editTask, setEditTask] = useState({
    description: '',
      category: 'Select',
      amount: '',
      endDate: ''
  })
  const [start, setStart] = useState(false);
  let initial;
  const handleButtonPress = (item) => {
    if(!start){
    initial =  setTimeout(() => {
      if(item.amount+limit>50000){
        setOpen(true);
        setTimeout(()=>{
          setOpen(false)
        }, 3000)
      }
      else{
      setStart(true); // start long button 
      if(!longPressedTasks.some(val => val.description === item.description)){
        setLongPressedTasks([...longPressedTasks, item])
        setLimit(limit+item.amount)
      }
    }
      }, 1500);
    }
    props.longPressedBill(longPressedTasks);
  }
  
  const handleButtonRelease = (item) => {
    if(!start) { // is click
        setStart(false); // stop long press   
        clearTimeout(initial); // clear timeout  
    } else {
      if(item.amount+limit>50000){
        setOpen(true);
        setTimeout(()=>{
          setOpen(false)
        }, 3000)
      }
      else{
        if (limit<50000 && longPressedTasks.length>0){
            if(!longPressedTasks.some(val => val.description === item.description)){
                setLongPressedTasks([...longPressedTasks, item])
            } else {
                var array = [...longPressedTasks]; // make a separate copy of the array
                var index = array.indexOf(item)
                if (index !== -1) {
                    array.splice(index, 1);
                    setLongPressedTasks(array);
                }
                if(longPressedTasks.length===0) {
                  setStart(false);
                }
            }
            props.longPressedBill(longPressedTasks);
        }
      }
    }
    
     
  }

  const handleFilter = (valueSearched) => {
    const filterObj = filterTasks.filter((item)=>(item.category.toString().includes(valueSearched)))
    props.filterBill(filterObj);
  };

  const handleClose = () => {
    setOpenModal(false);
  };

  const editTaskFunction = (index, description, category, amount, endDate) => {
    setEditTask({
      description: description,
      category: category,
      amount: amount,
      endDate: endDate
    });
    setEditIndex(index);
    setOpenModal(true);
  };

  const deleteTask = (description) => {
    
    const finalObj = props.items.filter((item)=>item.description!==description);
    props.deleteBill(finalObj);

  };

  const handleSubmit = (description, category, amount, endDate, editIndex) => {
    if(editIndex===-1){
      const finalObj = {
        description: description,
        category: category,
        amount: amount,
        endDate: endDate
      }
      setfilterTasks([...props.items, finalObj]);
      props.addBill(finalObj)
    } else {
      props.items[editIndex].description = description;
      props.items[editIndex].category = category;
      props.items[editIndex].amount = amount;
      props.items[editIndex].endDate = endDate;
      props.editBill(props.items);
    }
  };
    return (
       <div>
        <div id="top-Section" style={myStyle}>
            <FormControl style={width100}>
              <InputLabel>{constants.Search}</InputLabel>
              <Input
                name="accountName"
                style={width90}
                type="text"
                onChange={event =>{
                  valueSearched.current = event.target.value;
                  handleFilter(valueSearched.current)
                }
                }
                value={valueSearched.current}
              />
            </FormControl>
            <ButtonComponent style={{}} color="primary" text={constants.Add} click={()=>{
              setEditTask({
                description: '',
                category: '',
                amount: '',
                endDate: ''
              })
              setEditIndex(-1);
              // setNavigation(true)}}
              setOpenModal(true)}}
              />
              
            {props.items.length>0 &&
            <ButtonComponent text={navigate?'Back':'Timeline'} style={{marginLeft: 20}} color="primary" click={(item)=>{
              setNavigation(!navigate)}}
              />
              
            }
                
        </div>
          {editTask.description.length>0?
          <SimpleModalEdit
          openModal={openModal}
          description={editTask.description}
          category={editTask.category}
          amount={editTask.amount}
          handleSubmit={handleSubmit}
          handleClose={handleClose} 
          index= {editIndex}
          buttonText={editTask.description.length>0?'Edit Task':'Add Task'}
          endDate={editTask.endDate}/>
          :
          <SimpleModalAdd
          openModal={openModal}
          description={editTask.description}
          category={editTask.category}
          amount={editTask.amount}
          handleSubmit={handleSubmit}
          handleClose={handleClose} 
          index= {editIndex}
          buttonText={editTask.description.length>0?'Edit Task':'Add Task'}
          endDate={editTask.endDate}/>}
          {navigate?
      <OppositeContentTimeline />:
        <div>
            
          <div>

            {props.items.length>0?props.items.map((item, index)=>
            {return  <div
                onMouseDown={() => {handleButtonPress(item)}} 
                onMouseUp={()=>{handleButtonRelease(item)}}
            id="addTask" style={props.longpressed.some(val => val.description === item.description)?allTaskLongPressed:allTask}>
              <Typography>{item.description}</Typography>
              <Typography>{item.category}</Typography>
              <Typography>{item.amount}</Typography>
              <Typography>{item.endDate}</Typography>
            <div>
              <ButtonComponent style={{}} color="default" click={()=>editTaskFunction(index, item.description, item.category, item.amount, item.endDate)} text={constants.EditTask} />
                  
              <ButtonComponent style={margin20} color="secondary" click={()=>deleteTask(item.description)} text={constants.DeleteTask} />
                
            </div>
            </div>}
            ): <Typography style={noItem}>{constants.NoItem}</Typography>}
       
        </div>
        <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="info">
          {constants.LimitExceed}
        </Alert>
      </Snackbar>
        </div>
        }
        </div>
    )


}
const myStyle = {display: "flex",flex: 1, justifyContent: "space-between", flexDirection: "row", width:"90%", marginLeft: 40, marginTop: 40};
const width100 = {width: "100%"};
const width90 = {width: "90%"};
const allTask = {
  marginLeft: 40, marginTop: 20, flex: 1, justifyContent: "space-between", display: "flex", 
  alignItems: "center",
  paddingLeft: 10,
  paddingTop: 10,
  paddingBottom: 10,
  marginRight: 40,
  paddingRight: 10,
  borderRadius: 10,
  border: '2px solid rgba(0,0,0,0.2)'};

  const allTaskLongPressed = {
    marginLeft: 40, marginTop: 20, flex: 1, justifyContent: "space-between", display: "flex", 
    alignItems: "center",
    paddingLeft: 10,
    paddingTop: 10,
    paddingBottom: 10,
    marginRight: 40,
    paddingRight: 10,
    borderRadius: 10,
    border: '2px solid rgba(42, 187, 155, 1)'};

const margin20 = {marginLeft: 20};
const noItem = {display: "flex", justifyContent: "center", marginTop: 40};

const mapStateToProps = state => ({
    items: state.posts.items,
    longpressed: state.posts.longPressed
})

export default connect(mapStateToProps, {addBill, editBill, longPressedBill, deleteBill, filterBill})(Tasks)
