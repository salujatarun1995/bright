export const ADD_BILL = 'ADD_BILL';
export const DELETE_BILL = 'DELETE_BILL';
export const FILTER_BILL = 'FILTER_BILL';
export const EDIT_BILL = 'EDIT_BILL';
export const LONG_PRESSED = 'LONG_PRESSED';
