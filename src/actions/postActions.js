import { ADD_BILL, DELETE_BILL, EDIT_BILL, FILTER_BILL, LONG_PRESSED } from "./types";

export const addBill = (payloadValue) => dispatch => {
        dispatch({
            type: ADD_BILL,
            payload: payloadValue
        })
}

export const deleteBill = (payloadValue) => dispatch => {
    dispatch({
        type: DELETE_BILL,
        payload: payloadValue
    })
}

export const editBill = (payloadValue) => dispatch => {
    dispatch({
        type: EDIT_BILL,
        payload: payloadValue
    })
}

export const filterBill = (payloadValue) => dispatch => {
    dispatch({
        type: FILTER_BILL,
        payload: payloadValue
    })
}

export const longPressedBill = (payloadValue) => dispatch => {
    dispatch({
        type: LONG_PRESSED,
        payload: payloadValue
    })
}
