import { ADD_BILL, DELETE_BILL,FILTER_BILL, EDIT_BILL, LONG_PRESSED } from "../actions/types";

const initialState = {
    items: [],
    longPressed: [],
    item: {}
}

export default function(state=initialState, action){
    switch(action.type){
        case ADD_BILL: 
            return {
                ...state,
                items: [...state.items,action.payload]
            }
            break;
        case EDIT_BILL:
            return {
                ...state,
                items: action.payload
            };
            break;
        case LONG_PRESSED:
            return {
                ...state,
                longPressed: action.payload
            };
            break;
        case DELETE_BILL:
            return {
                ...state,
                items: action.payload
            };
            break;
        case FILTER_BILL:
            return {
                ...state,
                items: action.payload
            };
            break;
        default:
            return state;
            break;
    }
}